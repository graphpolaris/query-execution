# Query Execution Package
This is the package used by the [query service]() to execute queries on the database.

## Executor Interface
Any new executor needs to implement the `Executor` interface in `interface.go`.

## Creating an Executor
```go
import "git.science.uu.nl/datastrophe/query-execution/arangodb"

arangodbService := arangodb.NewService()
```

## Executing a Query
```go
queryResult, err := arangodbService.ExecuteQuery(query string, databaseUsername string, databasePassword string, databaseHost string, databasePort int, internalDatabase string)
```