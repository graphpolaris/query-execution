/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package queryexecution

import (
	"encoding/json"
	"errors"
)

/*
A MockService implements the Executor interface (mock)
*/
type MockService struct {
	throwError bool
}

/*
NewMockService creates a new service (mock)
	Return: *Mockservice, the mock new service
*/
func NewMockService() *MockService {
	return &MockService{
		throwError: false,
	}
}

/*
ExecuteQuery sends the query to a database and parses the result (mock)
	query: string, the query being sent
	databaseUsername: string, the username of the database
	databasePassword: string, the password of the database
	databaseHost: string, the host of the database
	databasePort: int, the port of the database
	internalDatabase: string, the internal database
	Return: (*[]byte, error), the query result and an error if there is one
*/
func (s *MockService) ExecuteQuery(query string, databaseUsername string, databasePassword string, databaseHost string, databasePort int, internalDatabase string) (*[]byte, error) {
	mockResult, _ := json.Marshal("test")

	if !s.throwError {
		return &mockResult, nil
	}
	return nil, errors.New("Database error")
}

/*
ToggleError decides whether the convert function throws an error
*/
func (s *MockService) ToggleError() {
	s.throwError = !s.throwError
}
