/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package queryexecution

import "git.science.uu.nl/graphpolaris/go-common/structs/v1/models"

/*
An Executor executes queries on a database
*/
type Executor interface {
	ExecuteQuery(query string, databaseInfo *models.DBConnectionModel) (*[]byte, error)
}
