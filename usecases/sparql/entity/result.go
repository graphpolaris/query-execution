/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entitysparql

/*
Defines the result type of the JSON received as a response when sending a query to a SPARQL dataset
standard by https://www.w3.org/TR/2013/REC-sparql11-results-json-20130321/
*/
type Result struct {
	Head    Select
	Results map[string][]map[string]Binding
}

type Select struct {
	Vars []string
}

type Bindings struct {
	Values interface{}
}

type Binding struct {
	Type  string
	Value string
}

/*
Maps a relation (e.g. r1) to the two entities it's connceted to (e.g. e0,e1)
This is our neccesary metadata for executing a query and converting the output into our standard format
*/
type MetaData struct {
	Triples map[string]Tuple
}

/*
Go has no tuples, so we make one ourselves specifically for SPARQL subject-predicate-object relation types
*/
type Tuple struct {
	From string
	To   string
}

/*
This is only used for duplicate checking in a roundabout manner, since entity.Edge cannot be used as a key for a map, since it contains interface{}
*/
type DuplicateCheckingTriple struct {
	Rel  string
	From string
	To   string
}
