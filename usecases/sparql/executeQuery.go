/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package sparql

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	"git.science.uu.nl/graphpolaris/query-execution/entity"
	entitysparql "git.science.uu.nl/graphpolaris/query-execution/usecases/sparql/entity"
)

/*
ExecuteQuery executes the given query on an ArangoDB instance
Currently DOES NOT actually implement the Executor interface, since it requires a different set of parameters
Since this code is essentially a proof-of-concept/template-to-be-extended,
 there is no reason to mess with the current, and working, interface definition and its implementations
	query: string, the query to be executed
	endpoint: string, the public SPARQL endpoint to query
	incomingTriplesJSON: string, the triples used in the query corresponding to their select identifiers
	Return: (*[]byte, error), returns the result of the query and a potential error
*/
func (s *Service) ExecuteQuery(query string, incomingTriplesJSON string, endpoint string) (*[]byte, error) {

	// TODO: Actually query a dataset rather than doing it from this mocked JSON file of data
	// You can probably curl the endpoint, sample code is provided in the "curl" function

	// Create data structure to store query result in
	// This means after converting the database response to our standard format
	nodeList := make([]entity.Node, 0)
	edgeList := make([]entity.Edge, 0)
	queryResult := make(map[string]interface{})
	// Unmarshal metaData into struct, query service can also pass an object instead of string I think?
	// TODO: Make query service send the new IncomingTranslation object rahter than strings
	metaData := entitysparql.MetaData{}
	err := json.Unmarshal([]byte(incomingTriplesJSON), &metaData)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(metaData)

	// Just a dummy function that reads and unmarshals our mock data
	// It goes into a entitysparql.Result format
	result := openAndRead(endpoint)
	// The ?e and ?r items get put into their respective lists
	selectedEntities, selectedRelations, err := getSelects(&result)
	if err != nil {
		fmt.Println(err)
	}
	// Maps to avoid duplicates
	nodeDoneMap := make(map[string]bool)
	relDoneMap := make(map[entitysparql.DuplicateCheckingTriple]bool)
	for i, binding := range result.Results["bindings"] {
		// First just make the nodes
		for _, e := range selectedEntities {
			// Avoid putting in duplicate nodes
			if _, ok := nodeDoneMap[binding[e].Value]; !ok {
				node := entity.Node{}
				attributes := make(map[string]interface{})
				attributes[binding[e].Type] = binding[e].Value
				node.ID = binding[e].Value
				node.Attributes = attributes
				nodeList = append(nodeList, node)
				nodeDoneMap[binding[e].Value] = true
			}
		}
		// Then do relations
		for j, r := range selectedRelations {
			// Funny way of turning two digits into a unique number
			id := fmt.Sprintf("%v%v", i, j)
			if err != nil {
				return nil, err
			}
			edge := entity.Edge{}
			attributes := make(map[string]interface{})
			// This is basically giving the edge the "uri" attribute
			attributes[binding[r].Type] = binding[r].Value
			edge.ID = id
			edge.Attributes = attributes
			// metaData.Triples is our map that allows us to find the entities a relation is connected to
			// That way we can get access to the ID (key[].Value) that we have to connect to
			for _, potentialE := range selectedEntities {
				if potentialE == metaData.Triples[r].From {
					edge.From = binding[potentialE].Value

				} else if potentialE == metaData.Triples[r].To {
					edge.To = binding[potentialE].Value
				}
			}
			// We can't use a map[edge]bool so we create this quick triple to check if a specific relation is already present
			// If it is, we can skip it
			// Not very clean code, but since go has no generics this will have to do
			// If we had generics then attributes wouldn't be interface{} and it would be possible to use it as a key for a map
			tempTriple := entitysparql.DuplicateCheckingTriple{
				From: edge.From,
				To:   edge.To,
				Rel:  binding[r].Value,
			}
			// Only add the result if we don't already have it
			if _, ok := relDoneMap[tempTriple]; !ok {
				edgeList = append(edgeList, edge)
				relDoneMap[tempTriple] = true
			}

		}
	}
	// Doesn't add rows for GroupBy at the moment
	queryResult["nodes"] = nodeList
	queryResult["edges"] = edgeList
	jsonQ, err := json.Marshal(queryResult)
	return &jsonQ, err

}

/*
curl sends a http POST request to a SPARQL endpoint and decodes the JSON response into a struct
	query: string, the query to be executed
	endpoint: string, the public SPARQL endpoint to query
	target: interface{}, the struct that should be filled with JSON data, this parameter is mutated during the function
	Return: error, returns either a http or JSON related error
*/
func curl(query string, endpoint string, target interface{}) error {
	// you can run this curl command to query an endpoint
	// curl -X POST http://35.233.212.30/blazegraph/namespace/kb/sparql --data "query=SELECT * { ?s ?p ?o } LIMIT 10" --data "format=json"
	// or in more general format
	// curl -X POST endpoint --data query --data "format=json"

	params := url.Values{}
	params.Add("query", query)
	params.Add("format", `json`)
	body := strings.NewReader(params.Encode())

	req, err := http.NewRequest("POST", endpoint, body)
	if err != nil {
		// handle err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
	}
	defer resp.Body.Close()
	return json.NewDecoder(resp.Body).Decode(target)

}

/*
getSelects splits the items preceded by the SELECT statement into two lists, one for entities and one for relations
	queryResult: *entitysparql.Result, contains all selected variables
	Return: ([]string, []string, error), the first list contains all entity variable names, the second list contains all relation variable names, the error in case a variable name is not recognized
*/
func getSelects(queryResult *entitysparql.Result) ([]string, []string, error) {
	selectedEntities := make([]string, 0)
	selectedRelations := make([]string, 0)
	for _, selectedItem := range queryResult.Head.Vars {
		if rune(selectedItem[0]) == 'e' {
			selectedEntities = append(selectedEntities, selectedItem)
		} else if rune(selectedItem[0]) == 'r' {
			selectedRelations = append(selectedRelations, selectedItem)
		} else {
			return selectedEntities, selectedRelations, errors.New("unrecognized var name")
		}
	}
	return selectedEntities, selectedRelations, nil
}

/*
openAndRead reads the mock data from a JSON file
	Return: entitysparql.Result, the formatted results from the database
*/
func openAndRead(file string) entitysparql.Result {
	jsonFile, err := os.Open(file)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	// read our opened jsonFile as a byte array.
	var result entitysparql.Result
	byteValue, _ := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(byteValue, &result)
	if err != nil {
		fmt.Println(err)
	}
	return result
}
