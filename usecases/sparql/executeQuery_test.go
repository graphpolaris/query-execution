/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package sparql

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var s *Service

/*
Creates the service
*/
func createService() {
	if s != nil {
		return
	}
	s = NewService()
}

/*
Tests if the executor properly runs using the mock data
	t: *testing.T, makes go recognize this as a test
*/
func TestExecution(t *testing.T) {
	createService()
	triples := `{
		"triples": {
			"r0": {
				"from": "e0",
				"to": "e1"
			},
			"r1": {
				"from": "e2",
				"to": "e1"
			}
		}
	}`
	_, err := s.ExecuteQuery("mock-query", triples, "../../main/sparql-mock-result.json")
	assert.Nil(t, err)
}
