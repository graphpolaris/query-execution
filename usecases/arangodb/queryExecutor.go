/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package arangodb

/*
Service implements the Executor interface
*/
type Service struct {
}

/*
NewService creates a new ArangoDB executor service

	Return: *Service, the new service
*/
func NewService() *Service {
	return &Service{}
}
