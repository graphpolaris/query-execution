/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entityarangodb

/*
Document with Empty struct to retrieve all data from the DB Document
*/
type Document map[string]interface{}

/*
GeneralFormat with Empty struct to retrieve all data from the DB Document
*/
type GeneralFormat map[string][]Document

/*
ListContainer is a struct that keeps track of the nodes and edges that need to be returned
*/
type ListContainer struct {
	NodeList []Document
	EdgeList []Document
	RowList  []Document
}
