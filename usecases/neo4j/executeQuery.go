/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package neo4j

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	entityneo4j "git.science.uu.nl/graphpolaris/query-execution/usecases/neo4j/entity"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/db"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/dbtype"
	"golang.org/x/exp/slices"
)

/*
ExecuteQuery executes the given query on an ArangoDB instance

	query: string, the query to be executed
	databaseUsername: string
	databasePassword: string
	databaseHost: string, URL for the database
	databasePort: int
	internalDatabase: string, the internal database to use, not all databases require this
	Return: (*[]byte, error), the query result and an error if there is one
*/
func (s *Service) ExecuteQuery(query string, databaseInfo *models.DBConnectionModel) (*[]byte, error) {

	// Connect to the database
	dbURI := fmt.Sprintf("%v%v:%v", databaseInfo.Protocol, databaseInfo.URL, databaseInfo.Port)

	driver, err := neo4j.NewDriver(dbURI, neo4j.BasicAuth(databaseInfo.Username, databaseInfo.Password, ""))
	if err != nil {
		log.Printf("Error connecting to NEO4J instance %s Err=%v", dbURI, err)
		return nil, err
	}

	defer driver.Close()

	// Create a session
	session := driver.NewSession(neo4j.SessionConfig{
		DatabaseName: databaseInfo.InternalDatabaseName,
		AccessMode:   neo4j.AccessModeRead,
	})
	if err != nil {
		log.Printf("Error creating session to NEO4J instance %s %s Err=%v", dbURI, databaseInfo.InternalDatabaseName, err)
		return nil, err
	}
	defer session.Close()

	// Detach marker from query
	// queries := strings.Split(query, ";")
	// queryCypher := queries[0]
	// returnType := queries[1]
	queryCypher := query     // TODO!
	returnType := "nodelink" // TODO!

	result, err := sendQuery(session, queryCypher)

	res, ok := result.([]*db.Record)
	if !ok {
		log.Printf("Parsing data from database failed %s %s", queryCypher, returnType)
		return nil, errors.New("parsing failed")
	}

	// Parse the database result
	var parsedResult interface{}

	switch returnType {
	case "nodelink":
		parsedResult, err = parseNodeLinkQuery(res)
		if err != nil {
			log.Printf("Nodelink failed %v", err)
			return nil, err
		}
	case "table":
		parsedResult, err = parseGroupByQuery(res)
		if err != nil {
			log.Printf("GroupBy failed %v", err)
			return nil, err
		}
	default:
		log.Printf("Error Unknown query Format %v", err)
		return nil, errors.New("unknown query format")
	}

	// Create the right return format
	endResult := make(map[string]interface{})
	endResult["type"] = returnType
	endResult["payload"] = parsedResult

	jsonResult, _ := json.Marshal(endResult)

	return &jsonResult, err
}

// parseNodeLinkQuery transforms the result into a node- and edgelist
func parseNodeLinkQuery(results []*db.Record) (map[string]interface{}, error) {
	nodes := make([]entityneo4j.Node, 0)
	edges := make([]entityneo4j.Edge, 0)
	nodeIds := make([]string, 0)
	edgeIds := make([]string, 0)
	result := make(map[string]interface{})

	for _, rec := range results {
		for _, res := range rec.Values {
			switch fmt.Sprintf("%T", res) {
			case "dbtype.Relationship":
				neoEdge, err := parseEdge(res)
				if err != nil {
					return nil, err
				}
				if !slices.Contains(edgeIds, neoEdge.ID) {
					edgeIds = append(edgeIds, neoEdge.ID)
					edges = append(edges, neoEdge)
				}
			case "dbtype.Node":
				neoNode, err := parseNode(res)
				if err != nil {
					return nil, err
				}
				if !slices.Contains(nodeIds, neoNode.ID) {
					nodeIds = append(nodeIds, neoNode.ID)
					nodes = append(nodes, neoNode)
				}
			case "dbtype.Path":
				path, ok := res.(dbtype.Path)
				if !ok {
					return nil, errors.New("invalid record type")
				}

				for _, node := range path.Nodes {
					neoNode, err := parseNode(node)
					if err != nil {
						return nil, err
					}
					if !slices.Contains(nodeIds, neoNode.ID) {
						nodeIds = append(nodeIds, neoNode.ID)
						nodes = append(nodes, neoNode)
					}
				}
				for _, edge := range path.Relationships {
					neoEdge, err := parseEdge(edge)
					if err != nil {
						return nil, err
					}
					if !slices.Contains(edgeIds, neoEdge.ID) {
						edgeIds = append(edgeIds, neoEdge.ID)
						edges = append(edges, neoEdge)
					}
				}
			default:
				// log.Printf("Ignoring Type: %T", res)
				// return nil, errors.New("invalid return found")
			}
		}
	}

	result["nodes"] = nodes
	result["edges"] = edges

	return result, nil
}

// parseNode transforms the dbtype.Node to a entityneo4j.Node
func parseNode(nodeInterf interface{}) (entityneo4j.Node, error) {
	node, ok := nodeInterf.(dbtype.Node)
	if !ok {
		return entityneo4j.Node{}, errors.New("invalid record type")
	}

	var newNode entityneo4j.Node
	newNode.ID = fmt.Sprintf("%v", node.Id)
	newNode.Attributes = node.Props
	newNode.Attributes["labels"] = node.Labels
	return newNode, nil
}

// parseEdge transforms the dbtype.Relationship to a entityneo4j.Edge
func parseEdge(edgeInterf interface{}) (entityneo4j.Edge, error) {
	edge, ok := edgeInterf.(dbtype.Relationship)
	if !ok {
		return entityneo4j.Edge{}, errors.New("invalid record type")
	}

	var newEdge entityneo4j.Edge
	newEdge.ID = fmt.Sprintf("%v", edge.Id)
	newEdge.From = fmt.Sprintf("%v", edge.StartId)
	newEdge.To = fmt.Sprintf("%v", edge.EndId)
	newEdge.Attributes = edge.Props
	newEdge.Attributes["Type"] = edge.Type
	return newEdge, nil
}

// parseGroupByQuery retrieves the tuples of the result and parses them into two lists
func parseGroupByQuery(results []*db.Record) (map[string]interface{}, error) {
	var groupKey, byKey string
	group := make([]string, 0)
	by := make([]string, 0)
	result := make(map[string]interface{}, 0)

	if len(results[0].Keys) != 2 {
		return nil, errors.New("invalid result format")
	}

	// Checks if the first letter of the first key is either an e or an r, since that would make it the By-key
	// By-keys start with e0_attr or r0_attr and group keys with an aggregation funcion such as AVG_attr or MIN_attr
	// This will work when an aggregation function starts with an E or R, since e != E
	if (results[0].Keys[0][0] == 'e') || (results[0].Keys[0][0] == 'r') {
		byKey = results[0].Keys[0]
		groupKey = results[0].Keys[1]
	} else {
		byKey = results[0].Keys[1]
		groupKey = results[0].Keys[0]
	}

	for _, res := range results {
		g, ok := res.Get(groupKey)
		if !ok {
			continue
		}

		b, ok := res.Get(byKey)
		if !ok {
			continue
		}

		group = append(group, fmt.Sprintf("%v", g))
		by = append(by, fmt.Sprintf("%v", b))
	}

	// Form proper result structure
	gMap := make(map[string]interface{}, 0)
	bMap := make(map[string]interface{}, 0)

	gMap["name"] = groupKey
	bMap["name"] = byKey
	gMap["values"] = group
	bMap["values"] = by

	result["group"] = gMap
	result["by"] = bMap

	return result, nil
}

// sendQuery sends a readonly query given a session
func sendQuery(session neo4j.Session, query string) (interface{}, error) {
	result, err := session.ReadTransaction(func(transaction neo4j.Transaction) (interface{}, error) {
		result, err := transaction.Run(query, nil)
		if err != nil {
			return nil, err
		}

		// Collects all results and puts them in []*db.Record format
		res, err := result.Collect()
		if err != nil {
			return nil, err
		}

		return res, nil
	})
	if err != nil {
		return nil, err
	}

	return result, err
}
