package neo4j

import (
	"fmt"
	"testing"

	"github.com/neo4j/neo4j-go-driver/v4/neo4j/db"
	"github.com/neo4j/neo4j-go-driver/v4/neo4j/dbtype"
	"github.com/stretchr/testify/assert"
)

// Some queries that will help debugging in the future:

// func TestRemote(t *testing.T) {

// 	// query := `MATCH p0 = (e0:Person)-[:DIRECTED*1..1]-(e1:Movie)
// 	// 	WHERE  e0.name <> "Raymond Campbell"
// 	// 	UNWIND relationships(p0) as r0
// 	// 	RETURN  e0, r0, e1
// 	// 	LIMIT 20;nodelink`

// 	// query := `MATCH (e0:Person)-[:DIRECTED*1..1]-(e1:Movie)
// 	// 	WHERE  e0.name <> "Raymond Campbell"
// 	// 	WITH *
// 	// 	MATCH (e0:Person)-[:ACTED_IN*1..1]-(e3:Movie)
// 	// 	WITH [e1, e3] as coll
// 	// 	UNWIND coll as e2
// 	// 	RETURN DISTINCT e2;nodelink`
// 	// lENGTH = 3322

// 	// query := `MATCH (e0:Person)-[:DIRECTED*1..1]-(e1:Movie)
// 	// 	WHERE  e0.name <> "Raymond Campbell"
// 	// 	WITH *
// 	// 	MATCH (e0:Person)-[:ACTED_IN*1..1]-(e3:Movie)
// 	// 	WITH [e1, e3] as coll
// 	// 	UNWIND coll as e2
// 	// 	RETURN e2;nodelink`
// 	// Length of nodelist = 20964

// 	// query := `MATCH (e0:Person)-[:DIRECTED*1..1]-(e1:Movie)
// 	// 	WHERE  e0.name <> "Raymond Campbell"
// 	// 	WITH *
// 	// 	MATCH (e0:Person)-[:ACTED_IN*1..1]-(e3:Movie)
// 	// 	WITH [e0] as coll
// 	// 	UNWIND coll as e2
// 	// 	RETURN e2;nodelink`
// 	// // LENGTH 10482
// 	query := `MATCH p1 = (e12:Movie)-[:ACTED_IN*1..1]-(e11:Person)
// 	UNWIND relationships(p1) as r11
// 	WITH *
// 	MATCH p0 = (e11:Person)-[:DIRECTED*1..1]-(e12:Movie)
// 	UNWIND relationships(p0) as r10
// 	WITH *
// 	UNWIND [r10,e11,e12,r11,e12,e11] AS x
// 	RETURN DISTINCT x
// 	Limit 10;nodelink`
// 	// LENGTH 487

// 	// query := `MATCH p0 = (e0:Person)-[:ACTED_IN*1..1]-(e1:Movie)
// 	// 	UNWIND relationships(p0) as r0
// 	// 	WITH *
// 	// 	WITH e0.bornIn AS e0_bornIn, AVG(e1.imdbRating) AS AVG_imdbRating
// 	// 		WHERE  AVG_imdbRating > 7.5
// 	// 	RETURN e0_bornIn, AVG_imdbRating
// 	// 	limit 10;table`

// 	hostname := "151e734c.databases.neo4j.io"
// 	port := 7687
// 	username := "neo4j"
// 	password := "DikkeDraak"
// 	internalDatabaseName := "Test"

// 	answer, err := NewService().ExecuteQuery(query, username, password, hostname, port, internalDatabaseName)
// 	if err != nil {
// 		fmt.Println(err)
// 		fmt.Println("couldnt reach database")
// 		t.Fail()
// 		return
// 	}
// 	// fmt.Println("The answer:")
// 	fmt.Println(" ")
// 	// fmt.Println(*answer)
// 	fmt.Println(string(*answer))
// 	fmt.Println("yeet")
// 	t.Fail()
// }

func TestNodeLink(t *testing.T) {

	n := dbtype.Node{
		Id:     3,
		Labels: []string{"Person"},
		Props:  make(map[string]interface{}),
	}
	var i0, i1 []interface{}
	i0 = append(i0, n)

	r := dbtype.Relationship{
		Id:      2,
		StartId: 3,
		EndId:   3,
		Type:    "Likes",
		Props:   make(map[string]interface{}),
	}

	i1 = append(i1, r)

	result := []*db.Record{
		{
			Keys:   []string{"x"},
			Values: i0,
		},
		{
			Keys:   []string{"x"},
			Values: i1,
		},
	}

	parsedRes, err := parseNodeLinkQuery(result)
	if err != nil {
		t.Fail()
	}

	answer := "map[edges:[{2 3 3 map[Type:Likes]}] nodes:[{3 map[labels:[Person]]}]]"
	assert.Equal(t, answer, fmt.Sprint(parsedRes))

}

func TestNodeParsing(t *testing.T) {
	n := dbtype.Node{
		Id:     3,
		Labels: []string{"Person"},
		Props:  make(map[string]interface{}),
	}

	var i interface{}
	i = n

	node, err := parseNode(i)
	if err != nil {
		t.Fail()
	}

	answer := "{3 map[labels:[Person]]}"
	assert.Equal(t, answer, fmt.Sprint(node))

}
func TestEdgeParsing(t *testing.T) {
	r := dbtype.Relationship{
		Id:      2,
		StartId: 3,
		EndId:   3,
		Type:    "Likes",
		Props:   make(map[string]interface{}),
	}

	edge, err := parseEdge(r)
	if err != nil {
		t.Fail()
	}

	answer := "{2 3 3 map[Type:Likes]}"
	assert.Equal(t, answer, fmt.Sprint(edge))
}
func TestGroupbyParsing(t *testing.T) {
	var i0, i1 []interface{}
	val0 := []string{"groen", "3000"}
	val1 := []string{"blauw", "4000"}
	i0 = append(i0, val0[0], val0[1])
	i1 = append(i1, val1[0], val1[1])

	result := []*db.Record{
		{
			Keys:   []string{"e0_Jaskleur", "AVG_inkomen"},
			Values: i0,
		},
		{
			Keys:   []string{"e0_Jaskleur", "AVG_inkomen"},
			Values: i1,
		},
	}

	parsedRes, err := parseGroupByQuery(result)
	if err != nil {
		t.Fail()
	}

	answer := "map[by:map[name:e0_Jaskleur values:[groen blauw]] group:map[name:AVG_inkomen values:[3000 4000]]]"
	assert.Equal(t, answer, fmt.Sprint(parsedRes))
}

func TestPathParsing(t *testing.T) {
	n := dbtype.Node{
		Id:     3,
		Labels: []string{"Person"},
		Props:  make(map[string]interface{}),
	}
	n2 := dbtype.Node{
		Id:     4,
		Labels: []string{"Person"},
		Props:  make(map[string]interface{}),
	}

	r := dbtype.Relationship{
		Id:      2,
		StartId: 3,
		EndId:   4,
		Type:    "Likes",
		Props:   make(map[string]interface{}),
	}

	p := dbtype.Path{
		Nodes:         []dbtype.Node{n, n2},
		Relationships: []dbtype.Relationship{r},
	}

	var i []interface{}
	i = append(i, p)

	result := []*db.Record{
		{
			Keys:   []string{"x"},
			Values: i,
		},
	}

	parsedRes, err := parseNodeLinkQuery(result)
	if err != nil {
		t.Fail()
	}

	answer := "map[edges:[{2 3 4 map[Type:Likes]}] nodes:[{3 map[labels:[Person]]} {4 map[labels:[Person]]}]]"
	assert.Equal(t, answer, fmt.Sprint(parsedRes))
}

func TestDuplicateParsing(t *testing.T) {
	n := dbtype.Node{
		Id:     3,
		Labels: []string{"Person"},
		Props:  make(map[string]interface{}),
	}
	n2 := dbtype.Node{
		Id:     3,
		Labels: []string{"Person"},
		Props:  make(map[string]interface{}),
	}

	r := dbtype.Relationship{
		Id:      2,
		StartId: 3,
		EndId:   3,
		Type:    "Likes",
		Props:   make(map[string]interface{}),
	}

	p := dbtype.Path{
		Nodes:         []dbtype.Node{n, n2},
		Relationships: []dbtype.Relationship{r},
	}

	var i []interface{}
	i = append(i, p)

	result := []*db.Record{
		{
			Keys:   []string{"x"},
			Values: i,
		},
	}

	parsedRes, err := parseNodeLinkQuery(result)
	if err != nil {
		t.Fail()
	}

	answer := "map[edges:[{2 3 3 map[Type:Likes]}] nodes:[{3 map[labels:[Person]]}]]"
	assert.Equal(t, answer, fmt.Sprint(parsedRes))
}
