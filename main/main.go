/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package main

import (
	"fmt"
	"log"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"git.science.uu.nl/graphpolaris/query-execution/usecases/arangodb"
	"git.science.uu.nl/graphpolaris/query-execution/usecases/sparql"
)

/* main is used to test the different queryExecutors */
func main2() {
	queryservice := arangodb.NewService()

	connection := &models.DBConnectionModel{
		URL:                  "https://datastrophe.science.uu.nl/",
		Port:                 8529,
		Username:             "root",
		Password:             "DikkeDraak",
		InternalDatabaseName: "TweedeKamer",
	}
	js := `LET result = (
		FOR e_29 IN parliament
		LET e30 = (
			FOR e_30 IN parties
			FOR r28 IN member_of
			FILTER r28._from == e_29._id AND r28._to == e_30._id
			FILTER length(e_30) != 0 AND length(r28) != 0
			RETURN {"nodes":union_distinct([e_30], []), "rel": union_distinct([r28], []), "mod": union_distinct([], [])}
		)
		FILTER length(e30) != 0 AND length(e_29) != 0
		RETURN {"nodes":union_distinct(flatten(e30[**].nodes), [e_29]), "rel": union_distinct(flatten(e30[**].rel), []), "mod": union_distinct(flatten(e30[**].mod), [])}
	)
	LET nodes = union_distinct(flatten(result[**].nodes),[])
	LET edges = union_distinct(flatten(result[**].rel),[])
	RETURN {"vertices":nodes,"edges":edges}`

	result, err := queryservice.ExecuteQuery(js, connection)
	log.Println(err)
	log.Println(string(*result))
}

func main() {
	queryservice := sparql.NewService()
	trips := `{
		"triples": {
			"r0": {
				"from": "e0",
				"to": "e1"
			},
			"r1": {
				"from": "e2",
				"to": "e1"
			}
		}
	}`
	res, err := queryservice.ExecuteQuery("lol", trips, "sparql-mock-result.json")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(*res))
}
