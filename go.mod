module git.science.uu.nl/graphpolaris/query-execution

go 1.22.0

toolchain go1.22.2

require (
	git.science.uu.nl/graphpolaris/go-common v0.0.0-20240927181628-a85c9246754f
	github.com/arangodb/go-driver v1.6.4
	github.com/neo4j/neo4j-go-driver/v4 v4.4.7
	github.com/stretchr/testify v1.9.0
	golang.org/x/exp v0.0.0-20241009180824-f66d83c29e7c
)

require (
	github.com/arangodb/go-velocypack v0.0.0-20200318135517-5af53c29c67e // indirect
	github.com/bmatcuk/doublestar/v4 v4.7.1 // indirect
	github.com/casbin/casbin/v2 v2.100.0 // indirect
	github.com/casbin/govaluate v1.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/qiangmzsx/string-adapter/v2 v2.2.0 // indirect
	github.com/rs/zerolog v1.33.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
