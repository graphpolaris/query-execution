/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

/*
IncomingTranslation contains the translated query and some possible metadata,
 the metadata should have a clearly defined type for each usecase.
 Each usecase should be responsible for unmarshalling to their own type
 This really is a usecase for generics, but Go doesn't have them yet.
*/
type IncomingTranslation struct {
	Translation string
	MetaData    interface{}
}
