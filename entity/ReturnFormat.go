/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

/*
Node is a JSON format for nodes
*/
type Node struct {
	ID         string                 `json:"id"`
	Attributes map[string]interface{} `json:"attributes"`
}

/*
Edge is a JSON format for edges
*/
type Edge struct {
	ID         string                 `json:"id"`
	From       string                 `json:"from"`
	To         string                 `json:"to"`
	Attributes map[string]interface{} `json:"attributes"`
}

/*
ResultFormat is a format for the result, the interface{} can be either an edge or a node
*/
type ResultFormat map[string][]interface{}
